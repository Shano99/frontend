import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfilePageWidgets extends StatefulWidget {
  @override
  _ProfilePageWidgetsState createState() => _ProfilePageWidgetsState();
}

class _ProfilePageWidgetsState extends State<ProfilePageWidgets> {
  List<String> test=["Test1","Test2","Test3"];
  List<String> score = ['10','20','30'];

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 10,),
          Center(
            child: CircleAvatar(
              child: Icon(Icons.person,size: 50,),
              radius: 60.0,
            ),
          ),
          SizedBox(
            height: 10
            ,),
          Center(
            child: Text("14180213",
              style: Theme.of(context).textTheme.headline6,),
          ),
          SizedBox(height: 10,),

          Center(child: SizedBox(child:Divider(thickness: 1.3,),width: 240,)),
          SizedBox(height: 10,),
          Container(
              padding:EdgeInsets.only(left: 10),child: Text("Previous Test Score!",style: Theme.of(context).textTheme.headline6,)),
          SizedBox(height: 10,),
          new Expanded(child: ListView.builder(
              itemCount: score.length,
              itemBuilder: (context,index)=>
              new Card(
                elevation: 1,
                shape: RoundedRectangleBorder(
                  side: BorderSide(color: Colors.black, width: 1.5),
                  borderRadius: BorderRadius.circular(10.0),
                ),
                child: Container(

                  padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),

                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(test[index],style: Theme.of(context).textTheme.subtitle1,),

                      Text(score[index],style: Theme.of(context).textTheme.subtitle1,),
                    ],
                  ),
                ),
              )))
        ],
      ),
    );
  }
}
