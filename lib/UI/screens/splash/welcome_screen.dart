import 'package:Sumedha/UI/screens/auth/login_screen.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';

class WelcomeScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    ScreenSize screenSize = ScreenSize(context);
    return Container(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(top: 70),
            child: SizedBox(
              height: screenSize.height / 5,
              child: Center(
                child: Image.asset(
                  LogoStringConstants.logo,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
          Container(
            decoration: BoxDecoration(),
            padding: EdgeInsets.fromLTRB(30, 50, 30, 0),
            child: Text(
              'Welcome ',
              textAlign: TextAlign.center,
              style: TextStyle(
                  letterSpacing: 1.25,
                  height: 1.4,
                  fontSize: 24,
                  fontWeight: FontWeight.w500),
            ),
          ),
          SizedBox(
            height: screenSize.height / 11,
          ),
          Container(
            child: Padding(
              padding: EdgeInsets.fromLTRB(60, 20, 60, 16),
              child: SizedBox(
                height: 50,
                child: RaisedButton(
                  padding: EdgeInsets.symmetric(horizontal: 50, vertical: 10),
                  color: Colors.black,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(18.0),
                  ),
                  onPressed: () {
                    Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (_) => LoginScreen()));
                  },
                  child: Text(
                    'Click to Login',
                    style: TextStyle(color: Colors.white, fontSize: 14),
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
