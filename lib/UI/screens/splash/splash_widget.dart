import 'package:Sumedha/providers/theme_changer.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';

class SplashWidget extends StatelessWidget {
  const SplashWidget({
    Key key,
    @required this.theme,
    @required this.screenSize,
  }) : super(key: key);

  final Themer theme;
  final ScreenSize screenSize;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Center(
          child: Hero(
            tag: 'logo',
            child: Container(
              margin: EdgeInsets.only(top: screenSize.height / 3.2),
              child: SizedBox(
                height: screenSize.height / 5,
                child: Image.asset(
                  LogoStringConstants.logo,
                  fit: BoxFit.fitHeight,
                ),
              ),
            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(bottom: 20),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              Padding(
                padding: const EdgeInsets.only(bottom: 15.0),
                child: Text(
                  'Developed By :',
                  textAlign: TextAlign.center,
                  style: Theme.of(context)
                      .textTheme
                      .headline6
                      .copyWith(fontWeight: FontWeight.w300),
                ),
              ),
              Image.asset(
                theme.logo,
                height: 50,
                width: 150,
              ),
            ],
          ),
        )
      ],
    );
  }
}
