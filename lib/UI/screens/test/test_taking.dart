import 'package:Sumedha/UI/components/app_drawer.dart';
import 'package:Sumedha/UI/components/appbar.dart';
import 'package:Sumedha/UI/screens/test/question_card_widget.dart';
import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/providers/exam_provider.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class TestTakingScreen extends StatefulWidget {
  final String subjectName, testName, slug;

  final Duration testDuration;
  final int totalQuestions;
  const TestTakingScreen(
      {Key key,
      @required this.subjectName,
      @required this.testName,
      @required this.testDuration,
      @required this.totalQuestions,
      @required this.slug})
      : super(key: key);
  @override
  _TestTakingScreenState createState() => _TestTakingScreenState();
}

class _TestTakingScreenState extends State<TestTakingScreen> {
  bool isSubmitting = false;
  bool isWaitingForNextQuestion = false;
  bool isSubmittingTest = false;

  @override
  Widget build(BuildContext context) {
    final examProvider = Provider.of<ExamProvider>(context);
    return WillPopScope(
      onWillPop: () {
        showDialogBox(
            context: context,
            title: 'Exam has started',
            message: 'Finish the exam to exit');
        return;
      },
      child: Scaffold(
        drawer: IgnorePointer(child: AppDrawer()),
        appBar: SumedhaAppBar(
          ignoreTouches: true,
        ),
        body: Consumer<QuestionsProvider>(
            builder: (context, questionProvider, child) {
          return FutureBuilder<Questions>(
              future: questionProvider.getAllQuestions(slug: widget.slug),
              builder: (context, snapshot) {
                if (!snapshot.hasData)
                  return Center(
                    child: CircularProgressIndicator(),
                  );
                List<Widget> questionCards = [];
                snapshot.data.exam.questionSet.forEach((question) {
                  questionCards.add(
                    QuestionCard(
                      questionSet: question,
                    ),
                  );
                });

                return isSubmittingTest
                    ? Center(
                        child: Column(
                          children: [
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text(
                                'Submitting test',
                                style: Theme.of(context).textTheme.headline6,
                              ),
                            ),
                            CircularProgressIndicator(),
                          ],
                        ),
                      )
                    : SingleChildScrollView(
                        child: Container(
                          alignment: Alignment.center,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 32,
                              ),
                              Container(
                                margin: EdgeInsets.only(left: 16, right: 16),
                                child: Row(
                                  children: [
                                    GestureDetector(
                                      onTap: () {
                                        showDialogBox(
                                            context: context,
                                            title: 'Exam has started',
                                            message: 'Finish the exam to exit');
                                      },
                                      child: Container(
                                        padding: const EdgeInsets.only(
                                            left: 24, right: 32),
                                        child: FaIcon(
                                          FontAwesomeIcons.caretLeft,
                                          size: 35,
                                        ),
                                      ),
                                    ),
                                    Flexible(
                                      child: Text(
                                        snapshot.data.exam.name,
                                        softWrap: false,
                                        textAlign: TextAlign.center,
                                        overflow: TextOverflow.fade,
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline6,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              Text(snapshot.data.exam.description,
                                  style: Theme.of(context).textTheme.subtitle1),
                              // Padding(
                              //   padding: const EdgeInsets.fromLTRB(16.0, 20, 16, 2),
                              //   child: Text(
                              //     '14:49 minutes left',
                              //     style: Theme.of(context)
                              //         .textTheme
                              //         .bodyText2
                              //         .copyWith(color: Colors.red),
                              //   ),
                              // ),
                              questionProvider.isLastQuestion
                                  ? Padding(
                                      padding: const EdgeInsets.all(12.0),
                                      child: Text(
                                        'All questions attended',
                                        style: Theme.of(context)
                                            .textTheme
                                            .headline5,
                                      ),
                                    )
                                  : Text(
                                      (widget.totalQuestions -
                                                  questionProvider
                                                      .currentQuestion)
                                              .toString() +
                                          ' Questions left',
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2),
                              questionProvider.isLastQuestion
                                  ? Column(
                                      children: [
                                        SizedBox(
                                          height: 50,
                                        ),
                                        Text(
                                          'Submit test',
                                          style: Theme.of(context)
                                              .textTheme
                                              .subtitle1,
                                        ),
                                        RaisedButton(
                                          onPressed: () async {
                                            setState(() {
                                              isSubmittingTest = true;
                                            });

                                            await questionProvider.endExam();
                                            setState(() {
                                              isSubmittingTest = false;
                                            });
                                            examProvider.refreshExams();
                                            Navigator.pop(context);
                                          },
                                          child: Text('Submit'),
                                        )
                                      ],
                                    )
                                  : questionCards[
                                      questionProvider.currentQuestion],
                              !questionProvider.isLastQuestion
                                  ? isWaitingForNextQuestion
                                      ? CircularProgressIndicator()
                                      : Row(
                                          mainAxisSize: MainAxisSize.min,
                                          children: <Widget>[
                                            GestureDetector(
                                              onTap: () {
                                                questionProvider.prevQuestion();
                                              },
                                              child: FaIcon(
                                                FontAwesomeIcons.backward,
                                                size: 35,
                                              ),
                                            ),
                                            SizedBox(
                                              width: 56,
                                            ),
                                            GestureDetector(
                                              onTap: () async {
                                                String response = '';
                                                setState(() {
                                                  isWaitingForNextQuestion =
                                                      true;
                                                });

                                                response =
                                                    await questionProvider
                                                        .nextQuestion();

                                                if (response == 'timeout') {
                                                  await showDialogBox(
                                                      context: context,
                                                      title: 'Exam ended',
                                                      message:
                                                          'The last response is saved. Exam will end now.');

                                                  Navigator.of(context)
                                                      .pushNamedAndRemoveUntil(
                                                          '/home',
                                                          (Route route) =>
                                                              false);
                                                } else if (response ==
                                                    'success') {
                                                } else {}
                                                setState(() {
                                                  isWaitingForNextQuestion =
                                                      false;
                                                });
                                              },
                                              child: FaIcon(
                                                FontAwesomeIcons.forward,
                                                size: 35,
                                              ),
                                            ),
                                          ],
                                        )
                                  : SizedBox(),
                              isSubmitting
                                  ? CircularProgressIndicator()
                                  : SizedBox(),
                            ],
                          ),
                        ),
                      );
              });
        }),
      ),
    );
  }

  showDialogBox(
      {@required BuildContext context,
      @required String title,
      @required String message}) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
          );
        }).then((value) {});
  }
}
