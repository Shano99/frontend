import 'package:Sumedha/UI/screens/test/tests_page_widget.dart';
import 'package:Sumedha/models/exams.dart';
import 'package:Sumedha/models/student_exam.dart';
import 'package:Sumedha/providers/exam_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TestsPage extends StatefulWidget {
  @override
  _TestsPageState createState() => _TestsPageState();
}

class _TestsPageState extends State<TestsPage> {
  @override
  Widget build(BuildContext context) {
    final examProvider = Provider.of<ExamProvider>(context);

    return Scaffold(
        body: RefreshIndicator(
      onRefresh: () async {
        examProvider.refreshExams();
      },
      child: Column(
        children: [
          SizedBox(
            height: 16,
          ),
          FutureBuilder<List<Exam>>(
              future: examProvider.getAllExams(),
              builder: (context, snapshot) {
                if (snapshot.hasData &&
                    snapshot.connectionState == ConnectionState.done) {
                  if (snapshot.data.length == 0) {
                    return SizedBox(
                      height: 16,
                    );
                  }
                  return Flexible(
                    child: ListView.builder(
                      shrinkWrap: true,
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, int index) {
                        if (index == 0) {
                          return Column(
                            children: [
                              Text(
                                'Pending',
                                style: Theme.of(context).textTheme.headline6,
                              ),
                              TestCard(
                                exam: snapshot.data[index],
                              )
                            ],
                          );
                        }
                        return TestCard(
                          exam: snapshot.data[index],
                        );
                      },
                    ),
                  );
                } else
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
              }),
          FutureBuilder<List<StudentExam>>(
              future: examProvider.getStudentExams(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length == 0) {
                    return SizedBox(
                      height: 16,
                    );
                  }
                  return Flexible(
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (context, int index) {
                        if (index == 0) {
                          return Column(
                            children: [
                              Divider(),
                              Text(
                                'Completed',
                                style: Theme.of(context).textTheme.headline6,
                              ),
                              StudentExamCard(
                                exam: snapshot.data[index],
                              )
                            ],
                          );
                        }
                        return StudentExamCard(
                          exam: snapshot.data[index],
                        );
                      },
                    ),
                  );
                } else
                  return Padding(
                    padding: const EdgeInsets.symmetric(vertical: 16.0),
                    child: Center(
                      child: CircularProgressIndicator(),
                    ),
                  );
              }),
        ],
      ),
    ));
  }
}
