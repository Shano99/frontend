import 'package:Sumedha/UI/screens/test/test_taking.dart';
import 'package:Sumedha/models/exams.dart';
import 'package:Sumedha/models/student_exam.dart';
import 'package:Sumedha/providers/questions_provider.dart';
import 'package:Sumedha/utilities/screen_size.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';

class TestCard extends StatefulWidget {
  final Exam exam;
  TestCard({@required this.exam});

  @override
  _TestCardState createState() => _TestCardState();
}

class _TestCardState extends State<TestCard> {
  bool isFetching = false;
  bool isSelected = false;
  @override
  Widget build(BuildContext context) {
    final questions = Provider.of<QuestionsProvider>(context);
    final ScreenSize screenSize = ScreenSize(context);
    return GestureDetector(
      onTap: () async {
        setState(() {
          isFetching = true;
        });

        await showDialog(
            context: context,
            builder: (BuildContext context) {
              return AlertDialog(
                title: Text('Warning'),
                content: Text(
                    'Once you start the exam you cannot exit. Please make sure you have a proper internet connection before starting.In case of any errors during the exam please contact the faculty concerned.'),
                actions: [
                  FlatButton(
                      onPressed: () {
                        isSelected = false;
                        Navigator.pop(context);
                        return false;
                      },
                      child: Text('No')),
                  FlatButton(
                      onPressed: () async {
                        Navigator.pop(context);
                        isSelected = true;

                        return true;
                      },
                      child: Text('Yes')),
                ],
              );
            }).then((value) {});
        if (isSelected) {
          await questions.getAllQuestions(slug: widget.exam.slug).then((value) {
            questions.reFetchQuestions();
            questions.startExam(value);
          });

          Navigator.push(
              context,
              MaterialPageRoute(
                  builder: (_) => TestTakingScreen(
                        slug: widget.exam.slug,
                        totalQuestions: widget.exam.questionsCount,
                        testDuration: Duration(minutes: 14, seconds: 44),
                        subjectName: widget.exam.name,
                        testName: widget.exam.description,
                      )));
        }
        setState(() {
          isFetching = false;
        });
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(20, 16, 20, 16),
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.black, width: 1.5),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 24, right: 24),
              child: FaIcon(FontAwesomeIcons.book, size: 35, color: Colors.red),
            ),
            Expanded(
              flex: 2,
              child: Container(
                height: screenSize.height / 6,
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        widget.exam.name,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: Text(
                        widget.exam.description,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: isFetching
                          ? Center(
                              child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: LinearProgressIndicator(),
                            ))
                          : Text(
                              'Total questions : ' +
                                  widget.exam.questionsCount.toString(),
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class StudentExamCard extends StatefulWidget {
  final StudentExam exam;
  StudentExamCard({@required this.exam});

  @override
  _StudentExamCardState createState() => _StudentExamCardState();
}

class _StudentExamCardState extends State<StudentExamCard> {
  bool isFetching = false;
  @override
  Widget build(BuildContext context) {
    final ScreenSize screenSize = ScreenSize(context);
    // final questions = Provider.of<QuestionsProvider>(context);
    return GestureDetector(
      onTap: () async {
        if (widget.exam.completed) {
          showDialogBox(
              context: context,
              title: widget.exam.name,
              message: 'Score : ' + widget.exam.score.toString());
        } else {
          showDialogBox(
              context: context,
              title: widget.exam.name,
              message: 'Test not completed');
        }
        // setState(() {
        //   isFetching = true;
        // });
        // await questions.getAllQuestions(slug: widget.exam.slug);
        // questions.startExam();
        // setState(() {
        //   isFetching = false;
        // });
        // Navigator.push(
        //     context,
        //     MaterialPageRoute(
        //         builder: (_) => TestTakingScreen(
        //               slug: widget.exam.slug,
        //               totalQuestions: widget.exam.questionsCount,
        //               testDuration: Duration(minutes: 14, seconds: 44),
        //               subjectName: widget.exam.name,
        //               testName: widget.exam.description,
        //             )));
      },
      child: Card(
        margin: EdgeInsets.fromLTRB(20, 16, 20, 16),
        elevation: 0,
        shape: RoundedRectangleBorder(
          side: BorderSide(color: Colors.black, width: 1.5),
          borderRadius: BorderRadius.circular(20),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 24, right: 24),
              child: FaIcon(FontAwesomeIcons.book, size: 35, color: Colors.red),
            ),
            Expanded(
              flex: 2,
              child: Container(
                height: screenSize.height / 6,
                padding: EdgeInsets.all(8),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      child: Text(
                        widget.exam.name,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: Text(
                        widget.exam.description,
                        overflow: TextOverflow.ellipsis,
                        softWrap: false,
                        style: Theme.of(context).textTheme.subtitle2,
                      ),
                    ),
                    SizedBox(
                      height: 8,
                    ),
                    Container(
                      child: isFetching
                          ? Center(
                              child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: LinearProgressIndicator(),
                            ))
                          : Text(
                              'Total questions : ' +
                                  widget.exam.questionsCount.toString(),
                              overflow: TextOverflow.ellipsis,
                              softWrap: false,
                              style: Theme.of(context).textTheme.bodyText1,
                            ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  showDialogBox(
      {@required BuildContext context,
      @required String title,
      @required String message}) async {
    await showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text(title),
            content: Text(message),
          );
        }).then((value) {});
  }
}
