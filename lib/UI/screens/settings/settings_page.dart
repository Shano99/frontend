import 'package:Sumedha/UI/common/themes/base_theme.dart';
import 'package:Sumedha/providers/theme_changer.dart';
import 'package:Sumedha/utilities/constants/enums.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  @override
  Widget build(BuildContext context) {
    final theme = Provider.of<Themer>(context);

    return Scaffold(
      body: ListView(
        children: <Widget>[
          Container(
            margin: EdgeInsets.all(8),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(
                  Icons.settings,
                  size: 40,
                ),
                Text(
                  'Settings',
                  style: Theme.of(context).textTheme.headline4,
                ),
              ],
            ),
          ),
          Divider(),
          Container(
            margin: EdgeInsets.all(8),
            child: SwitchListTile(
                title: Text('Dark Mode : '),
                value: theme.appTheme == AppTheme.Dark,
                onChanged: (v) {
                  if (v)
                    theme.setTheme(BaseTheme.darkTheme);
                  else
                    theme.setTheme(BaseTheme.lightTheme);
                }),
          ),
        ],
      ),
    );
  }
}
