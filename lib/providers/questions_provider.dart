import 'package:Sumedha/models/questions.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/cupertino.dart';

class QuestionsProvider extends ChangeNotifier {
  DataSource _dataSource = DataSource();
  Questions questions;
  int currentQuestion = 0;
  int totalQuestions = 0;
  String slug;

  bool fetchedQuestions = false, isNewExam = true;
  bool isFirstQuestion = true, isLastQuestion = false;

  StorageHelper _storageHelper = StorageHelper();
  List<MarkedAnswers> markedAnswersList = [];

  Future<Questions> getAllQuestions({@required String slug}) async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    questions = await _dataSource.getQuestions(access: access, slug: slug);

    totalQuestions = questions.exam.questionSet.length - 1;
    fetchedQuestions = false;
    isFirstQuestion = true;
    isLastQuestion = false;

    this.slug = slug;
    return questions;
  }

  void reFetchQuestions() async {
    fetchedQuestions = false;
    notifyListeners();
  }

  void startExam(Questions questions) {
    if (!fetchedQuestions) {
      markedAnswersList = [];
      questions.exam.questionSet.forEach((question) {
        markedAnswersList.add(MarkedAnswers(
            questionID: question.id,
            answerID: question.answerSet[0].id,
            selectedOption: 0));
      });
      totalQuestions = questions.exam.questionSet.length - 1;
      isFirstQuestion = true;
      isLastQuestion = false;
      fetchedQuestions = true;
    }
  }

  Future<bool> endExam() async {
    bool isTestSubmitted = false;
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    isTestSubmitted =
        await _dataSource.submitTest(access: access, slug: slug) ?? false;
    markedAnswersList = [];
    questions.exam.questionSet.forEach((question) {
      markedAnswersList
          .add(MarkedAnswers(questionID: 1, answerID: 0, selectedOption: 0));
    });
    currentQuestion = 0;
    isFirstQuestion = true;
    fetchedQuestions = false;
    isLastQuestion = false;
    totalQuestions = 0;
    slug = '';

    notifyListeners();
    return isTestSubmitted;
  }

  markAnswer(
      {@required int questionID, @required int answerID, int selectedOption}) {
    markedAnswersList.forEach((answer) {
      if (questionID == answer.questionID) {
        answer.selectedOption = selectedOption;
        answer.answerID = answerID;
      }
    });

    notifyListeners();
  }

  Future<String> nextQuestion() async {
    String submissionResponse = 'success';
    if (currentQuestion <= totalQuestions) {
      if (currentQuestion == totalQuestions) {
        await getNewAccessToken();

        String access =
            await _storageHelper.getString(AuthStringConstants.access);
        submissionResponse = await _dataSource.submitAnswer(
                slug: slug,
                access: access,
                questionID: markedAnswersList[currentQuestion].questionID,
                answerID: markedAnswersList[currentQuestion].answerID) ??
            'error';
        isLastQuestion = true;
        notifyListeners();
        return submissionResponse;
      }
      await getNewAccessToken();

      String access =
          await _storageHelper.getString(AuthStringConstants.access);
      submissionResponse = await _dataSource.submitAnswer(
              slug: slug,
              access: access,
              questionID: markedAnswersList[currentQuestion].questionID,
              answerID: markedAnswersList[currentQuestion].answerID) ??
          'error';
      currentQuestion++;
      notifyListeners();
      return submissionResponse;
    }
    return submissionResponse;
  }

  void prevQuestion() {
    if (currentQuestion == 0) {
      isFirstQuestion = true;
      isLastQuestion = false;
    }
    if (currentQuestion > 0) {
      currentQuestion--;
      isLastQuestion = false;
    } else {
      isLastQuestion = false;
      isFirstQuestion = true;
    }
    notifyListeners();
  }

  QuestionSet getQuestion(int questionNumber) {
    currentQuestion = questionNumber;
    return questions.exam.questionSet[questionNumber];
  }

  Future<void> getNewAccessToken() async {
    String refresh =
        await _storageHelper.getString(AuthStringConstants.refresh);
    await _dataSource.refresh(refresh).then((value) async => {
          await _storageHelper.saveString(
              key: AuthStringConstants.access, value: value.access)
        });
  }
}

class MarkedAnswers {
  int questionID, answerID, selectedOption;
  MarkedAnswers({this.answerID, this.questionID, this.selectedOption});
  @override
  String toString() {
    print(' Question id: ' +
        questionID.toString() +
        '\nAnswer Id : ' +
        answerID.toString() +
        '\n Selected option : ' +
        selectedOption.toString());
    return super.toString();
  }
}
