import 'package:Sumedha/models/category.dart';
import 'package:Sumedha/models/materials.dart';
import 'package:Sumedha/models/subject.dart';
import 'package:Sumedha/services/api_service.dart';
import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/storage_helper.dart';
import 'package:flutter/widgets.dart';

class SubjectProvider extends ChangeNotifier {
  DataSource _dataSource = DataSource();
  List<Subject> subjectList;
  List<Category> categoryList;
  List<Materials> materialList;
  StorageHelper _storageHelper = StorageHelper();
  Future<List<Subject>> getAllSubjects() async {
    if (subjectList == null) {
      await getNewAccessToken();
      String access =
          await _storageHelper.getString(AuthStringConstants.access);
      this.subjectList = await _dataSource.getAllSubjects(access);
      notifyListeners();
    }
    return this.subjectList;
  }

  Future<List<Category>> getCategory(int id) async {
    List<Category> categoryList;

    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    categoryList =
        await _dataSource.getSubjectCategories(id: id, access: access);
    categoryList = categoryList;

    return categoryList;
  }

  Future<List<Materials>> getMaterials(int id) async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    this.materialList =
        await _dataSource.getMaterialCategories(access: access, id: id);

    notifyListeners();

    return materialList;
  }

  Future<void> getNewAccessToken() async {
    String refresh =
        await _storageHelper.getString(AuthStringConstants.refresh);
    await _dataSource.refresh(refresh).then((value) async => {
          await _storageHelper.saveString(
              key: AuthStringConstants.access, value: value.access)
        });
  }

  Future<int> downloadMaterial(String url, String fileName) async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    return await _dataSource.downloadFile(
        downloadURL: url, access: access, fileName: fileName);
  }

  Future<int> downloadGet({String url, String fullPath}) async {
    return await _dataSource.downloadAsByte(url: url, fullPath: fullPath);
  }

  materialRead(String materialID) async {
    await getNewAccessToken();
    String access = await _storageHelper.getString(AuthStringConstants.access);
    await _dataSource.postMaterialRead(access: access, materialID: materialID);
  }

  void refreshSubjects() {
    this.subjectList = null;
    this.categoryList = null;
    this.materialList = null;
    notifyListeners();
  }
}
