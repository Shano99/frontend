import 'dart:convert';

import 'package:Sumedha/utilities/constants/string_constants.dart';
import 'package:Sumedha/utilities/constants/utility_fuctions.dart';

User userFromMap(String str) => User.fromMap(json.decode(str));

String userToMap(User data) => json.encode(data.toMap());

class User {
  User(
      {this.tokenType,
      this.exp,
      this.jti,
      this.userId,
      this.res,
      this.payload,
      this.jwt,
      this.refresh,
      this.access});

  String tokenType, access, refresh, jwt;
  DateTime exp;
  String jti;
  int userId;
  Map<String, dynamic> res, payload;

  User copyWith(
      {String tokenType,
      access,
      refresh,
      jwt,
      DateTime exp,
      String jti,
      int userId,
      Map<String, dynamic> res,
      payload}) {
    return User(
        tokenType: tokenType ?? this.tokenType,
        exp: exp ?? this.exp,
        jti: jti ?? this.jti,
        userId: userId ?? this.userId,
        access: access ?? this.access,
        refresh: refresh ?? this.refresh,
        jwt: jwt ?? this.jwt,
        res: res ?? this.res,
        payload: payload ?? this.payload);
  }

  factory User.fromAccessToken(
      User tempUser, Map<String, dynamic> accessToken) {
    User user = tempUser.copyWith(
      access: accessToken[AuthStringConstants.access],
    );

    return user;
  }

  factory User.fromMap(Map<String, dynamic> json) {
    Map<String, dynamic> data = UtilityFunction.tryParseJwt(json['access']);
    User tempUser = User(
      payload: data,
      refresh: json[AuthStringConstants.refresh],
      access: json[AuthStringConstants.access],
      res: json,
    );

    User user = tempUser.copyWith(
      tokenType: tempUser.payload["token_type"],
      exp: DateTime.fromMillisecondsSinceEpoch(tempUser.payload["exp"] * 1000),
      jti: tempUser.payload["jti"],
      userId: tempUser.payload["user_id"],
    );

    return user;
  }

  Map<String, dynamic> toMap() => {
        "token_type": tokenType,
        "exp": exp.millisecondsSinceEpoch,
        "jti": jti,
        "user_id": userId,
      };
}
