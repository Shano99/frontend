// To parse this JSON data, do
//
//     final materials = materialsFromMap(jsonString);

import 'dart:convert';

List<Materials> materialsFromMap(String str) =>
    List<Materials>.from(json.decode(str).map((x) => Materials.fromMap(x)));

String materialsToMap(List<Materials> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toMap())));

class Materials {
  Materials({
    this.id,
    this.file,
    this.fileName,
    this.name,
    this.fileType,
    this.fileLink,
    this.category,
    this.user,
  });

  int id;
  String file;
  String fileName;
  String name;
  int fileType;
  String fileLink;
  int category;
  int user;

  Materials copyWith({
    int id,
    String file,
    String fileName,
    String name,
    int fileType,
    String fileLink,
    int category,
    int user,
  }) =>
      Materials(
        id: id ?? this.id,
        file: file ?? this.file,
        fileName: fileName ?? this.fileName,
        name: name ?? this.name,
        fileType: fileType ?? this.fileType,
        fileLink: fileLink ?? this.fileLink,
        category: category ?? this.category,
        user: user ?? this.user,
      );

  factory Materials.fromMap(Map<String, dynamic> json) => Materials(
        id: json["id"],
        file: json["file"],
        fileName: json["file_name"],
        name: json["name"],
        fileType: json["file_type"],
        fileLink: json["file_link"],
        category: json["category"],
        user: json["user"],
      );

  Map<String, dynamic> toMap() => {
        "id": id,
        "file": file,
        "file_name": fileName,
        "name": name,
        "file_type": fileType,
        "file_link": fileLink,
        "category": category,
        "user": user,
      };
}
